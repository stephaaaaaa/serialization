﻿using System;
using System.Collections.Generic;
using System.Text;
[Serializable()]
class Course
{
    public String course_name { get; set; }
    public int course_id { get; set; }
    public int studentsEnrolled { get; set; }


    public Course(String name, int id)
    {
        course_name = name;
        course_id = id;
        studentsEnrolled = 0;
    }



}

