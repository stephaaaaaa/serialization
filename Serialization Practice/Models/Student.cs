﻿using System;
using System.Collections.Generic;
using System.Text;

[Serializable()]
public class Student
{
    public String f_name { get; set; }
    public String l_name { get; set; }
    public int studentID { get; }

    public Student(String first, String last, int ID)
    {
        f_name = first;
        l_name = last;
        studentID = ID;
    }

    public Student()
    {
        f_name = "???";
        l_name = "???";
        studentID = -1;
    }



}

