﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Xml.Serialization;

class Program
{
    static void Main(string[] args)
    {
        //Creating objects
        Student s0 = new Student("Mayson", "Green", 1457);
        Student s2 = new Student();
        s2.f_name = "Stephanie";
        s2.l_name = "Labastida";
        Course CS354 = new Course("Programming Languages", 354);

        JsonSerializer jsonSerializer = new JsonSerializer();
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(Student));

        /*
         *Serializing an object to json
         */
        using (StreamWriter sw = new StreamWriter(@"C:\Local Git Repo\Serialization Practice\Serialization Practice\Data\Test.json"))
        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            jsonSerializer.Serialize(writer, s0);
        }

        /*
         * Deserializing an object from json
         */
        string data = System.IO.File.ReadAllText(@"C:\Local Git Repo\Serialization Practice\Serialization Practice\Data\Test.json");
        Student mayson = JsonConvert.DeserializeObject<Student>(data);

        /*
         * Serializing an object to xml
         */
        using (StreamWriter sw = new StreamWriter(@"C:\Local Git Repo\Serialization Practice\Serialization Practice\Data\Test.xml"))
        xmlSerializer.Serialize(sw, s2);

        /*
         *Deserializing an xml object 
         */
        StreamReader reader = new StreamReader(@"C:\Local Git Repo\Serialization Practice\Serialization Practice\Data\Test.xml");
        Student xmlStephanie = (Student)xmlSerializer.Deserialize(reader);


        //Printing results
        Console.WriteLine("{0}, {1}", mayson.f_name, mayson.l_name);
        Console.WriteLine("{0}, {1}",xmlStephanie.f_name,xmlStephanie.l_name);
        Console.Read();

        //TEST
        
    }
}
